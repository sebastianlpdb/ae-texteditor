import React, {Component} from 'react';
import Modal from 'react-modal';
import './App.css';
import ControlPanel from "./control-panel/ControlPanel";
import FileZone from "./file-zone/FileZone";
import getMockText from './text.service';
import Model from './model/text';

const customStyles = {
    content : {
      top                   : '50%',
      left                  : '50%',
      right                 : 'auto',
      bottom                : 'auto',
      marginRight           : '-50%',
      transform             : 'translate(-50%, -50%)'
    }
  };
  
  // Make sure to bind modal to your appElement (http://reactcommunity.org/react-modal/accessibility/)
  Modal.setAppElement('#root')

class App extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            text: '',
            sourceText: [],
            selection: '',
            modalIsOpen: false,
            synonyms: []
        };

        this.Model = new Model();

        this.handleOnMouseUp = this.handleOnMouseUp.bind(this);
        this.handleActionButtonClick = this.handleActionButtonClick.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
        this.handleDoubleClick = this.handleDoubleClick.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.handleSynonymClick = this.handleSynonymClick.bind(this);
    }

    componentDidMount() {
        getMockText().then(text => {
            this.updateText(text);
        });
    }

    openModal() {
        this.setState({modalIsOpen: true});
    }
    
    closeModal() {
        this.setState({modalIsOpen: false});
    }
    
    handleOnMouseUp(selection) {
        this.setState({
            selection
        });
    }

    handleActionButtonClick(type) {
        const { selection } = this.state;

        if (selection) {
            this.Model.modifyWord(selection, type);
            this.setState({
                sourceText: this.Model.getText()
            })
        }
    }

    handleTextChange(text) {
        this.updateText(text);
    }

    handleDoubleClick(word) {
        fetch(`https://api.datamuse.com/words?ml=${word}&max=30`).then(response => response.json()).then(synonyms => {
            this.setState({
                synonyms,
            }, () => this.openModal());
        });
    }

    updateText(text) {
        this.Model.addText(text);
        
        this.setState({
            text,
            sourceText: this.Model.getText()
        });
    }

    handleSynonymClick(synonym) {
        const newText = this.state.text.replace(this.state.selection, synonym);
        this.updateText(newText);
        this.closeModal();
    }

    render() {
        return (
            <div className="App">
            <Modal
                isOpen={this.state.modalIsOpen}
                onRequestClose={this.closeModal}
                style={customStyles}
                contentLabel="Modal"
                className="Modal"
            >
                <p>Synonysm</p>
                <ul>
                    {this.state.synonyms.map(synonym => {
                        return <li key={synonym.word}><button onClick={() => {this.handleSynonymClick(synonym.word)}}>{synonym.word}</button></li>
                    })}
                </ul>
                <button onClick={this.closeModal}>Close Modal</button>
            </Modal>
                <header>
                    <span>Simple Text Editor</span>
                </header>
                <main>
                    <ControlPanel onActionButtonClick={this.handleActionButtonClick} />
                    <FileZone onDoubleClick={this.handleDoubleClick} onChange={this.handleTextChange} text={this.state.text} onMouseUp={this.handleOnMouseUp} sourceText={this.state.sourceText} />
                </main>
            </div>
        );
    }
}

export default App;
