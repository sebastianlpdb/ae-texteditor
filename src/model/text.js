export default class Text {
  constructor() {
    this.text = [];

    this.modifiers = {
      italic: {
        tag: 'i'
      },
      underline: {
        tag: 'u'
      },
      bold: {
        tag: 'strong'
      }
    }
  }

  addText(text) {
    const splitted = text.match(/\b(\w+\W+)/g);
    
    this.text = splitted.map(word => {
      return {
        word,
        modifiers: []
      };
    });
  }

  modifyWord(word, modifier) {
    this.text = this.text.map(textObject => {
      const withoutSpecialWord = textObject.word.replace(/[^a-zA-Z]/g, '');
      
      if (withoutSpecialWord === word) {
        if (!textObject.modifiers.includes(modifier)) {
          textObject.modifiers.push(modifier);
        } else {
          textObject.modifiers.splice(textObject.modifiers.indexOf(modifier), 1);
        }        
      }

      return textObject;
    });
  }

  getText() {
    return this.text;
  }

  setText(text) {
    this.text = text;
  }

  createMarkup() {
    let string = '<p>';
    for (let x = 0; x < this.text.length; x++) {
      const { word, modifiers } = this.text[x];
      for (let y = 0; y < modifiers.length; y++) {
        string += `<${this.modifiers[modifiers[y]].tag}>`;
      }
      string += `${word}`;
      for (let y = 0; y < modifiers.length; y++) {
        string += `</${this.modifiers[modifiers[y]].tag}>`;
      }
    }
    string += '</p>';

    return { __html: string };
  }
}