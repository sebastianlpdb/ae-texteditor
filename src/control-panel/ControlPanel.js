import React, { Component } from 'react';
import './ControlPanel.css';

const ControlPanel = props => {
    return (
        <div id="control-panel">
            <div id="format-actions">
                <button onClick={() => props.onActionButtonClick('bold')}className="format-action" type="button"><u>B</u></button>
                <button onClick={() => props.onActionButtonClick('italic')}className="format-action" type="button"><u>I</u></button>
                <button onClick={() => props.onActionButtonClick('underline')}className="format-action" type="button"><u>U</u></button>
            </div>
        </div>
    );
}

export default ControlPanel;
