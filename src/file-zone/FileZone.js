import React, { Component } from 'react';
import './FileZone.css';
import Model from '../model/text';

class FileZone extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isTextAreaVisible: false,
        }

        this.Model = new Model();

        this.textRef = React.createRef();

        this.handleOnMouseUp = this.handleOnMouseUp.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
        this.handleDoubleClick = this.handleDoubleClick.bind(this);
    }
    
    handleOnMouseUp() {
        const activeElement = this.textRef.current;

        const text = activeElement.value.slice(activeElement.selectionStart, activeElement.selectionEnd);

        this.props.onMouseUp(text);
    }

    handleTextChange() {
        const activeElement = this.textRef.current;

        const text = activeElement.value;

        this.props.onChange(text);
    }

    handleDoubleClick() {
        const activeElement = this.textRef.current;

        const word = activeElement.value.slice(activeElement.selectionStart, activeElement.selectionEnd);

        this.props.onDoubleClick(word);
    }

    render() {
        this.Model.setText(this.props.sourceText);

        return (
            <div className="container">
                <textarea
                    col="240"
                    rows="10"
                    className="text-area"
                    ref={this.textRef} 
                    onChange={this.handleTextChange} 
                    onMouseUp={this.handleOnMouseUp}
                    onDoubleClick={this.handleDoubleClick}
                    value={this.props.text}
                />
                <div className="preview--container" dangerouslySetInnerHTML={this.Model.createMarkup()} />
            </div>
        );
    }
}

export default FileZone;
